-- show the list of possible collations

SELECT *
FROM fn_helpcollations();

--search collations, using parameters

SELECT Name, Description FROM fn_helpcollations()
WHERE Name like 'Latin%' AND Description LIKE '% case-insensitive%';

--set collation on DB creation

CREATE DATABASE SomeDB
COLLATE SQL_Latin1_General_CP1_CI_AS;

-- alter DB collation (doesn't change table collations)
-- had problems: 
-- 1. delete CK (check constraint) on dbo.Country (No schema-bound object depends on the collation of the database!) 
-- 2. delete MSVTF

ALTER DATABASE [SQL Saturday]
COLLATE SQL_Latin1_General_CP1_CI_AS

--Verify the collation setting.

SELECT name, collation_name
FROM sys.databases
WHERE name = 'SQL Saturday';

--ALTER TABLE doesn't work generally. We have to specify (n)char/(n)varchar/(n)text columns:
-- before that - save scripts & drop indexes

ALTER TABLE [Address] 
ALTER COLUMN [City] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS

ALTER TABLE [Address] 
ALTER COLUMN [Street] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS

-- use COLLATION in sorting:

SELECT Country FROM dbo.Country
ORDER BY Country
COLLATE Albanian_CI_AS ASC;

SELECT Country FROM dbo.Country
ORDER BY Country
COLLATE Latvian_BIN;

SELECT ASCII(SUBSTRING(Country, 2, 1)), Country FROM dbo.Country WHERE Country like '%Islands%'