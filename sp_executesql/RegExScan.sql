ALTER PROCEDURE spSearchPersonalData
AS
BEGIN
	SET NOCOUNT on;
	IF OBJECT_ID('tempdb..##tempPersonalDataTable') IS NOT NULL
		DROP TABLE ##tempPersonalDataTable
	CREATE TABLE ##tempPersonalDataTable(sdSCHEMA nvarchar(40), sdTABLE nvarchar(40), sdCOLUMN nvarchar(40), Level varchar(12))

	INSERT INTO ##tempPersonalDataTable(sdSCHEMA, sdTABLE, sdCOLUMN, Level)
	SELECT s.TABLE_SCHEMA, t.name, c.name, Level = 'Critical'
	FROM INFORMATION_SCHEMA.TABLES s
	INNER JOIN sys.tables t
	ON s.TABLE_NAME = t.name
	INNER JOIN sys.columns c 
	ON t.object_id = c.object_id
	WHERE c.name LIKE '%[P,p]erson%' OR
		c.name LIKE '%[A,a]ddress%' OR
		c.name LIKE '%[C,c]ity%' OR
		c.name LIKE '%[S,s]tate%' OR
		c.name LIKE '%[D,d]ate%' OR
		c.name LIKE '%[C,c]redit%[C,c]ard%' OR
		c.name LIKE '%[C,c]redit%' OR
		c.name LIKE '%[C,c]ard%' OR
		--c.name LIKE '%[C,c][C,c]%' OR
		c.name LIKE '%[S,s]ocial%[S,s]ecurity%[N,n]umber%' OR
		c.name LIKE '%[S,s][S,s][N,n]%' OR
		c.name LIKE '%[S,s]%[S,s]%[N,n]um%' OR
		c.name LIKE '%[S,s]oc[S,s]ec%' OR
		c.name LIKE '%[A,a]ccount%' OR
		c.name LIKE '%[F,f]ax%' OR
		c.name LIKE '%[P,p]hone%' OR
		c.name LIKE '%[T,t]ele%[P,p]hone%' OR
		c.name LIKE '%[L,l]icense%' OR
		c.name LIKE '%[V,v]ehicle%' OR
		c.name LIKE '%[V,v][I,i][N,n]%' OR
		c.name LIKE '%[R,r]ecord%' OR
		c.name LIKE '%[P,p]atient%' OR
		c.name LIKE '%[B,b]ank%' OR
		c.name LIKE '%[R,r]oute%' OR
		c.name LIKE '%[P,p][I,i][N,n]%' OR
		c.name LIKE '%[N,n][A,a][M,m][E,e]%' OR

		--Medication
		--c.name LIKE '%[M,m]ed%[N,n]ame%' OR
		--c.name LIKE '%[D,d]rug%[N,n]ame%[F,f]ull%' OR
		c.name LIKE '%[G,g][P,p][I,i]%' OR
		--c.name LIKE '%[D,d]rug%[N,n]ame%' OR
		--c.name LIKE '%[G,g]eneric%[N,n]ame%' OR
		c.name LIKE '%[D,d]rug%[C,c]omments%' OR

		--Vitals
		c.name LIKE '%[R,r]ecorded%[D,d][T,t][M,m]%' OR
		c.name LIKE '%[V,v]alue%' OR
		c.name LIKE '%[D,d]escription%' OR
		c.name LIKE '%[U,u]nit%[O,o]f%[M,m]easure%' OR
		c.name LIKE '%[R,r]ight%[J,j]ustified%[L,l]abel%' OR
		c.name LIKE '%[L,l]eft%[J,j]ustified%[L,l]abel%' OR

		--Task
		--c.name LIKE '%[P,p]at%[N,n]ame%' OR
		c.name LIKE '%[D,d]escription%' OR
		c.name LIKE '%[T,t]ask%[O,o]wner%' OR

		--Patient
		--c.name LIKE '%[L,l]ast%[N,n]ame%' OR
		--c.name LIKE '%[F,f]irst%[N,n]ame%' OR
		--c.name LIKE '%[M,m]iddle%[N,n]ame%' OR
		c.name LIKE '%[G,g]ender%' OR
		c.name LIKE '%[A,a]ge%' OR
		c.name LIKE '%[P,p]atient%[P,p]ix%' OR
		c.name LIKE '%[E,e]thnicity%' OR
		c.name LIKE '%[L,l]anguage%' OR
		c.name LIKE '%[R,r]ace%' OR
		c.name LIKE '%[M,m]artial%[S,s]tatus%' OR
		c.name LIKE '%[M,m]%[R,r]%[N,n]%' OR
		c.name LIKE '%[E,e]mployer%' OR
		c.name LIKE '%[O,o]ccupation%' OR
		c.name LIKE '%[G,g]ender%[C,c]ode%' OR

		--PhoneTyte
		c.name LIKE '%[C,c]ell%[P,p]hone%' OR
		c.name LIKE '%[P,p]hone%[N,n]umber%' OR
		c.name LIKE '%[W,w]ork%[P,p]hone%' OR
		c.name LIKE '%[H,h]ome%[P,p]hone%' OR

		--Address
		c.name LIKE '%[Z,z]ip%[C,c]ode%' OR
		c.name LIKE '%[A,a]ddress%[L,l]ine%' OR

		--Visit
		--c.name LIKE '%[C,c]lient%[D,d]isplay%[N,n]ame%' OR
		c.name LIKE '%[V,v]sist%[S,s]tatus%' OR
		c.name LIKE '%[A,a]dmit%[D,d][T,t][M,m]%' OR
		c.name LIKE '%[D,d]ischarge%[D,d][T,t][M,m]%' OR
		--c.name LIKE '%[P,p]rovider%[D,d]isplay[N,n]ame%' OR
		c.name LIKE '%[V,v]isit%[R,r]eason%' OR
		c.name LIKE '%[S,s]ervice%[D,d]escription%' OR
		c.name LIKE '%[V,v]isit%[T,t]ype[C,c]ode%' OR
		c.name LIKE '%[V,v]isit%[C,c]are%[L,l]evel%[C,c]ode%' OR

		--Location
		c.name LIKE '%[L,l]ocation%[C,c]ode%' OR
		c.name LIKE '%[C,c]urrent%[L,l]ocation%[GUID,guid]%' OR
		--c.name LIKE '%[L,l]ocn%[G,g]roup[N,n]ame%' OR
		--c.name LIKE '%[E,e]ntry%[N,n]ame%' OR
		c.name LIKE '%[M,m]nemonic%'

		SELECT @@ROWCOUNT AS FOUND
	UPDATE ##tempPersonalDataTable
	SET Level = 'non-critical'
	WHERE sdCOLUMN LIKE '%[M,m]odif%[D,d]ate%' OR
		sdCOLUMN LIKE '%[V,v]er%[D,d]ate%' OR
		sdCOLUMN LIKE '%[O,o]rder%[D,d]ate%' OR
		sdCOLUMN LIKE '%[E,e]nd%[D,d]ate%' OR
		sdCOLUMN LIKE '%[S,s]tart%[D,d]ate%' OR
		sdCOLUMN LIKE '%[U,u]pdat%[D,d]ate%' OR
		sdCOLUMN LIKE '%[I,i]nsert%[D,d]ate%' OR
		sdCOLUMN LIKE '%[A,a]rchi%[D,d]ate%' OR
		sdCOLUMN LIKE '%[C,c]urren%[D,d]ate%' OR
		sdCOLUMN LIKE '%[D,d]eliver%[D,d]ate%' OR
		sdCOLUMN LIKE '%[R,r]eview%[D,d]ate%' OR
		sdCOLUMN LIKE '%[R,r]eceipt%[D,d]ate%' OR
		sdCOLUMN LIKE '%[D,d]ate%[C,c]reat%' OR
		sdCOLUMN LIKE '%[S,s]hip%[D,d]ate%'

		SELECT @@ROWCOUNT AS MODIFIED
	SELECT * FROM ##tempPersonalDataTable
	ORDER BY sdSCHEMA, sdTABLE, sdCOLUMN
END
GO

EXECUTE spSearchPersonalData
GO