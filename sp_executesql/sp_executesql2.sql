--EXEC

EXEC('SELECT * FROM dbo.Person')

--sp_executesql = system stored procedure, executes unicode string as SQL statement
--sp_executesql creates a plan + stores it in a cash (user wins in speed)

EXEC sp_executesql N'SELECT * FROM dbo.Person'

--dynamic sql

DECLARE @Tablename nvarchar(80)
DECLARE @SQLString nvarchar(MAX)

SET @Tablename = N'dbo.Person'
SET @SQLString = 'SELECT * FROM ' + @Tablename

EXECUTE sp_executesql @SQLString

--create stored procedure

CREATE PROC spVariableTable (
	@TableName NVARCHAR (40)
)
AS
BEGIN

	DECLARE @SQLString nvarchar(MAX)
	SET @SQLString = N'SELECT * FROM ' + @TableName
	EXEC sp_executesql SQLString

END

--use created procedure

EXEC spVariableTable 'sys.objects', 10
SELECT * FROM sys.objects
SELECT TOP 100 name FROM sysobjects WHERE name LIKE 'sys%'

-- change stored procedure

ALTER PROC spVariableTable (
	@TableName NVARCHAR (40),
	@Number int
)
AS
BEGIN

	DECLARE @SQLString nvarchar(MAX)
	DECLARE @NumberString nvarchar(4)
	SET @NumberString = CAST(@Number AS nvarchar(4))
	SET @SQLString = N'SELECT TOP ' + @NumberString + ' name FROM ' + @TableName + 
	' WHERE name LIKE ''sys%'''

	SELECT @SQLString
	EXEC sp_executesql @SQLString

END

-- all system tables: 1) starts from 'sys' 2) ID number < 100 3) type S in sysobjects table

SELECT name FROM sysobjects
WHERE type = 'v'
AND name LIKE 'sys%'

SELECT DISTINCT SUBSTRING(name,1,3) AS prefix, type FROM sysobjects
WHERE name LIKE '[a-z][a-z][_]%'
UNION ALL
SELECT DISTINCT name AS prefix, type FROM sysobjects
WHERE name NOT LIKE '[a-z][a-z][_]%'
