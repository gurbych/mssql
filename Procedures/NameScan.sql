ALTER PROCEDURE searchNames
	@db_name nvarchar(128)
AS
BEGIN
	DECLARE @s_name nvarchar(128)
	DECLARE @t_name nvarchar(128)
	DECLARE @c_name nvarchar(128)
	DECLARE @str nvarchar(MAX)

	DECLARE person_cursor CURSOR LOCAL FOR
		SELECT s.name, t.name, c.name
		FROM sys.tables t
		INNER JOIN sys.columns c
		ON c.object_id = t.object_id
		INNER JOIN sys.schemas s
		ON s.schema_id = t.schema_id
		AND t.name NOT LIKE 'sys%'
		WHERE system_type_id IN (35, 167, 175, 231, 239) 

	OPEN person_cursor
	FETCH NEXT FROM person_cursor INTO @s_name, @t_name, @c_name
	WHILE @@FETCH_STATUS=0
		BEGIN
			SELECT @s_name AS dbSCHEMA, @t_name AS dbTABLE, @c_name AS dbCOLUMN

			SELECT @str = N'SELECT COUNT(1) AS MATCHES FROM [' + @db_name + '].[' + @s_name + '].[' + @t_name + '] a
			INNER JOIN PopularDictionary.dbo.FirstName b
			ON a.[' + @c_name + '] = b.FirstName
			COLLATE SQL_Latin1_General_CP1_CI_AS'
			
			EXECUTE sp_executesql @str
			FETCH NEXT FROM person_cursor INTO @s_name, @t_name, @c_name
		END
	CLOSE person_cursor
	DEALLOCATE person_cursor
END

--EXECUTE searchNames 'AdventureWorks2012'