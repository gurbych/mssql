USE [SQL Saturday]
GO

/****** Object:  StoredProcedure [dbo].[listPersonByName]    Script Date: 06.02.2015 1:09:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[getPeople]
	@Name varchar(20) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
BEGIN TRY
	IF @Name IS NULL
		RAISERROR('Invalid name %s', 16, 1, @Name)
	ELSE
		SELECT Lastname, Firstname, Gender, DateOfBirth
		FROM dbo.Person
		WHERE Firstname = @Name;
END TRY
BEGIN CATCH
	PRINT 'I was called from catch block'
	DECLARE @ErrorMessage nvarchar(60);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE()
	RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END



GO

EXEC dbo.getPeople NULL

SELECT Lastname, Firstname, Gender, DateOfBirth
FROM dbo.Person
WHERE Firstname IS NULL;


SP_RENAME 'listPersonByName', 'getPeople'