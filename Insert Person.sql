USE [Students]
GO

INSERT INTO [dbo].[UkrainianStudents]
           ([FirstName]
           ,[LastName]
           ,[Email])
     VALUES
           ('Ivan',
           'Chernenko',
           'ivchern@ukr.net'),
		   ('Stepan',
           'Voevoda',
           'stepan@ukr.net'),
		   ('John',
           'Doe',
           'jd@gmail.com')
GO

INSERT INTO [dbo].[AmericanStudents]
           ([FirstName]
           ,[LastName]
           ,[Email])
     VALUES
           ('Sam',
           'Fisher',
           'samfisher@i.com'),
		   ('Roger',
           'Stanakis',
           'rs@bing.com'),
		   ('John',
           'Doe',
           'jd@gmail.com')
GO

