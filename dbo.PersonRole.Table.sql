USE [SQL Saturday]
GO
/****** Object:  Table [dbo].[PersonRole]    Script Date: 21.01.2015 18:42:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.PersonRole', N'U') IS NULL
CREATE TABLE [dbo].[PersonRole](
	[PersonID] [tinyint] IDENTITY(1, 1) NOT NULL,
	[RoleID] [tinyint] NOT NULL
) ON [PRIMARY]

GO
