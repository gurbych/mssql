USE [Students]
Go

-- UNION joins rows with equivalent data types (number and types of columns must match). 
-- Uses SELECT DISTINCT filter => only unique fields, slower than UNION ALL

SELECT 'American: ' AS Nationality, FirstName, LastName, Email FROM AmericanStudents
UNION
SELECT 'Ukrainian: ' AS Nationality,FirstName, LastName, Email FROM UkrainianStudents
ORDER BY FirstName

-- UNION ALL faster than UNION. No DISTINCT filter => shows duplicates

SELECT FirstName, LastName, Email FROM AmericanStudents
UNION ALL
SELECT FirstName, LastName, Email FROM UkrainianStudents

-- UNION changes order of fields chaotically, so we have to sort them:

SELECT FirstName, LastName, Email FROM AmericanStudents
UNION
SELECT FirstName, LastName, Email FROM UkrainianStudents
ORDER BY FirstName

-- INTERSECT = overlap of two sets

SELECT FirstName, LastName, Email FROM AmericanStudents
INTERSECT
SELECT FirstName, LastName, Email FROM UkrainianStudents

-- EXCEPT = left set - right set

SELECT FirstName, LastName, Email FROM AmericanStudents
EXCEPT
SELECT FirstName, LastName, Email FROM UkrainianStudents