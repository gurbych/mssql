USE [SQL Saturday]
GO

-- take a look what we have

SELECT ClassID, Title FROM Class
SELECT ScheduleID, Start_time, ClassID FROM Schedule

-- INNER JOIN = show matching rows only (do not show NULLs)

SELECT Start_time, Title -- columns from different tables
FROM Schedule
INNER JOIN Class
ON Class.ClassID=Schedule.ClassID -- specify JOIN condition (matching column = foreign key)
ORDER BY Start_time;

-- LEFT OUTER JOIN = show all mathing rows + all rows from left table (allow NULLs)

SELECT Start_time, Title
FROM Schedule
LEFT JOIN Class
ON Class.ClassID=Schedule.ClassID
ORDER BY Start_time;

-- show NULL values from left table

SELECT Start_time, Title
FROM Schedule
LEFT JOIN Class
ON Class.ClassID=Schedule.ClassID
WHERE Schedule.ClassID IS NULL

-- RIGHT OUTER JOIN = show all mathing rows + all rows from right table (allow NULLs)

SELECT Start_time, Title
FROM Schedule
RIGHT JOIN Class
ON Class.ClassID=Schedule.ClassID
ORDER BY Start_time;

-- show NULL values from right table

SELECT Start_time, Title
FROM Schedule
RIGHT JOIN Class
ON Class.ClassID=Schedule.ClassID
WHERE Class.Title IS NULL

-- FULL OUTER JOIN = 

SELECT Start_time, Title
FROM Schedule
FULL JOIN Class
ON Class.ClassID=Schedule.ClassID
ORDER BY Start_time;

-- show all NULLs

SELECT Start_time, Title
FROM Schedule
FULL JOIN Class
ON Class.ClassID=Schedule.ClassID
WHERE Class.Title IS NULL
OR Schedule.ClassID is NULL

-- CROSS JOIN = set CARTESIAN product

SELECT Start_time, Title
FROM Schedule
CROSS JOIN Class

SELECT P.Firstname, P.Lastname 
FROM Person P
CROSS JOIN Person P1