SELECT Start_time FROM dbo.Schedule

SELECT CAST(Start_time AS Date) FROM dbo.Schedule

SELECT CONVERT(Time, Start_time, 100) FROM dbo.Schedule
SELECT CONVERT(char(7), CAST(Start_time AS TIME), 100) FROM dbo.Schedule -- US Style (AM/PM)
SELECT CONVERT(char(7), CAST(Start_time AS TIME), 101) FROM dbo.Schedule
SELECT CONVERT(varchar, Start_time) AS 'Start Time' FROM dbo.Schedule -- US Style (full)
SELECT CONVERT(varchar, Start_time, 111) AS 'Start Time' FROM dbo.Schedule -- European style

--convert date into string

SELECT CONVERT(CHAR(10), GETDAte(), 101) --US style
SELECT CONVERT(CHAR(19), GETDATE(), 100) -- Default
-- get year (cheat)
SELECT CONVERT(CHAR(4), GETDATE(), 102) -- ANSI (yyyy.mm.dd)
--get month (cheat)
SELECT CONVERT(CHAR(3), GETDATE(), 0) -- Default (mm.dd.yy)
SELECT CONVERT(CHAR(2), GETDATE(), 101) --US style (mm.dd.yyyy)
--get day (cheat)
SELECT CONVERT(CHAR(2), GETDATE(), 103) -- British/French(dd.mm.yyyy)
--get day of the week
SELECT CONVERT(CHAR(50), GETDATE(), 107) -- doesnt work correctly

--get part of date using DATEPART() - returns INT

SELECT DATEPART(MONTH, GETDATE())
SELECT DATEPART(YEAR, GETDATE())
SELECT DATEPART(DAY, GETDATE())

--get part of date using DATENAME() - returns NVARCHAR

SELECT DATENAME(MONTH, GETDATE())
SELECT DATENAME(YEAR, GETDATE())
SELECT DATENAME(DAY, GETDATE())

SELECT GETDATE() 'Full date', YEAR(GETDATE()) 'Only year'
SELECT GETDATE() 'Full date', MONTH(GETDATE()) 'Only month'
SELECT GETDATE() 'Full date', DAY(GETDATE()) 'Only day'

SELECT DATENAME(DW, GETDATE())