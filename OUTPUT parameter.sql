CREATE PROCEDURE spGetEmployeeCount
@gender varchar(1),
@count int output
AS 
BEGIN 
SELECT @count = count(*)
FROM HumanResources.Employee
WHERE @gender = Gender
END
GO

DECLARE @total int
EXECUTE spGetEmployeeCount 'F', @total out
SELECT @total AS TOTAL
GO