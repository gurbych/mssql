--inner join on tables from 2 DB
SELECT COUNT(1)
FROM AdventureWorks2012.Person.Person a
INNER JOIN PopularDictionary.dbo.FirstName b
ON a.FirstName = b.FirstName
COLLATE SQL_Latin1_General_CP1_CI_AS

--query to find all current database names
SELECT * FROM sys.databases
SELECT name FROM sys.databases
EXEC sp_databases
SELECT DB_NAME() --current DB name

--select schema
SELECT DISTINCT TABLE_SCHEMA FROM INFORMATION_SCHEMA.COLUMNS -- all DB schemas

SELECT SCHEMA_NAME() --user's default schema name

SELECT * FROM  sys.schemas
WHERE principal_id = 1

SELECT * FROM sys.columns


		SELECT s.name, t.name, c.name
		FROM sys.tables t
		INNER JOIN sys.columns c
		ON c.object_id = t.object_id
		INNER JOIN sys.schemas s
		ON s.schema_id = t.schema_id
		AND t.name NOT LIKE 'sys%'
		WHERE system_type_id IN (35, 167, 175, 231, 239)