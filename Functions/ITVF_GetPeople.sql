USE [SQL Saturday]
GO

ALTER FUNCTION ITVF_GetPersons(@DOB Date)
RETURNS TABLE
AS
BEGIN TRY
RETURN (SELECT Person.PersonID, Lastname, Firstname, DateOfBirth AS Year FROM dbo.Person INNER JOIN PersonRole 
ON Person.PersonID = PersonRole.PersonID 
WHERE DateOfBirth > @DOB)
END TRY
BEGIN CATCH
RAISERROR('Invalid data type %s', 14, 1, @DOB)
END CATCH

SELECT * FROM ITVF_GetPersons(CAST('1/1/1900' AS Date))

UPDATE ITVF_GetPersons('1/1/1900') SET Firstname = 'AAAAAA' WHERE PersonID = 1