USE [SQL Saturday]
GO

CREATE FUNCTION MSTVF_GetPersons()
RETURNS @Table Table(ID smallint, GivenName varchar(20), DOB Date)
AS
BEGIN
	INSERT INTO @Table
	SELECT PersonID, Lastname, DateOfBirth FROM dbo.Person
	RETURN
END

SELECT * FROM MSTVF_GetPersons()

SELECT COUNT(*) FROM MSTVF_GetPersons()

SELECT MAX(DOB) FROM MSTVF_GetPersons()

SELECT MIN(DOB) FROM MSTVF_GetPersons()

SELECT AVG(ID) FROM MSTVF_GetPersons()

SELECT SUM(ID) FROM MSTVF_GetPersons()