USE [SQL Saturday]
GO
/****** Object:  Table [dbo].[Address]    Script Date: 21.01.2015 18:42:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF OBJECT_ID ('Address') IS NULL
CREATE TABLE [dbo].[Address](
	[AddressID] [smallint] IDENTITY(1, 1) NOT NULL,
	[CountryCode] [smallint] NOT NULL,
	[City] [varchar](100) NOT NULL,
	[Street] [varchar](255) NOT NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[AddressID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
