USE [SQL Saturday]
GO

/****** Object:  Index [unx_Address]    Script Date: 08.02.2015 14:18:08 ******/
CREATE UNIQUE NONCLUSTERED INDEX [unx_Address] ON [dbo].[Address]
(
	[CountryCode] ASC,
	[City] ASC,
	[Street] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


