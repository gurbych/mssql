--random uniqueidentifier

SELECT NEWID()

--GUID in a variable and stored procedure

DECLARE @SomeID uniqueidentifier
SET @SomeID = NEWID()
SELECT @SomeID

--insert in temporary table

CREATE PROCEDURE showMeGUID
AS
BEGIN
	CREATE TABLE #temp(ID uniqueidentifier, Name varchar(40))
	INSERT INTO #temp
	VALUES (NEWID(), 'Anatoly'), (NEWID(), 'Zahar'), (NEWID(), 'Ivan')
	SELECT * FROM #temp
END

EXECUTE showMeGUID