USE [SQL Saturday]
GO

/****** Object:  Index [unx_Role]    Script Date: 22.01.2015 17:48:12 ******/
CREATE UNIQUE NONCLUSTERED INDEX [unx_Schedule] ON [dbo].[Schedule]
(
	[Start_time] ASC, [AddressID] ASC, [ClassID] ASC, [RoomID] ASC, [PersonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


