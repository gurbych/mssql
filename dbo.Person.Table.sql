USE [SQL Saturday]
GO
/****** Object:  Table [dbo].[Person]    Script Date: 21.01.2015 18:42:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF OBJECT_ID (N'dbo.Person', N'U') IS NULL
CREATE TABLE [dbo].[Person](
	[PersonID] [smallint] IDENTITY(1, 1) NOT NULL,
	[Firstname] [char](20) NOT NULL,
	[Lastname] [char](20) NOT NULL,
	[Gender] [char](1) NOT NULL,
	[DateOfBirth] [date] NOT NULL,
 CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED 
(
	[PersonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
