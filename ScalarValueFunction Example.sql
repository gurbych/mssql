SELECT Firstname, dbo.CalculateAge(DateOfBirth) FROM dbo.Person

DROP FUNCTION CalculateAge
GO
CREATE FUNCTION CalculateAge(@DOB Date)
RETURNS int
AS 
BEGIN
DECLARE @Age int
SET @Age = DATEDIFF(YEAR, @DOB, GETDATE())
	IF (MONTH(@DOB) > MONTH(GETDATE()) 
	OR 
	(MONTH(@DOB) = MONTH(GETDATE()) 
	AND 
	DAY(@DOB) > DAY(GETDATE())
	)
	)
	SET @Age = 1
	ELSE 
	SET @Age = 0
RETURN @Age
END