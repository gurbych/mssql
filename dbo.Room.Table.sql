USE [SQL Saturday]
GO
/****** Object:  Table [dbo].[Room]    Script Date: 21.01.2015 18:42:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Room', N'U') IS NULL
CREATE TABLE [dbo].[Room](
	[RoomID] [tinyint] IDENTITY(1, 1) NOT NULL,
	[Capacity] [tinyint] NOT NULL,
	[Floor] [tinyint] NOT NULL,
	[Number] [int] NOT NULL,
 CONSTRAINT [PK_Room] PRIMARY KEY CLUSTERED 
(
	[RoomID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
