INSERT INTO dbo.Address (City, State, ZipCode, Addressline1)
VALUES
	('Huntsville', 'Alabama', 35801, '38 River Road'),
	('Los Angeles', 'California', 900001, '56 Main Street'),
	('Miami', 'Florida', 33124, '34 Union Street'),
	('Springfield', 'Illinois', 60601, '123 School Street')