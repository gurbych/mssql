SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF OBJECT_ID ('Vitals') IS NULL
CREATE TABLE [dbo].[Vitals](
	[VitalsID] [int] IDENTITY(1, 1) NOT NULL,
	[RecordedDTM] [smalldatetime] NOT NULL,
	[Value] [varchar](50) NOT NULL,
	[Description] [varchar](30) NOT NULL,
	[UnitOfMeasure] [varchar](6) NOT NULL,
	[RightJustifiedLabel] [varchar](30) NOT NULL,
	[LeftJustifiedLabel] [varchar](30) NOT NULL,
 CONSTRAINT [PK_Vitals] PRIMARY KEY CLUSTERED 
(
	[VitalsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
