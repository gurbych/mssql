SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID ('PatientPhysician') IS NULL
CREATE TABLE [dbo].[PatientPhysician](
	[PatientID] [int] IDENTITY(1, 1) NOT NULL,
	[PhysicianID] [smallint] NOT NULL
	FOREIGN KEY (PatientID) references Patient(PatientID),
	FOREIGN KEY (PhysicianID) references Physician(PhysicianID),
) ON [PRIMARY]

GO
