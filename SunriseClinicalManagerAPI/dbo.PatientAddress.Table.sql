SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID ('PatientAddress') IS NULL
CREATE TABLE [dbo].[PatientAddress](
	[PatientID] [int] IDENTITY(1, 1) NOT NULL,
	[AddressID] [smallint] NOT NULL
	FOREIGN KEY (PatientID) references Patient(PatientID),
	FOREIGN KEY (AddressID) references Address(AddressID),
) ON [PRIMARY]

GO
