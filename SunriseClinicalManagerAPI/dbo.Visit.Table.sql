SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF OBJECT_ID ('Visit') IS NULL
CREATE TABLE [dbo].[Visit](
	[VisitID] [int] IDENTITY(1, 1) NOT NULL,
	[PatientID] [int] NOT NULL,
	[ClientDisplayName] [varchar](40) NOT NULL,
	[VisitStatus] [varchar](20) NULL,
	[VisitReason] [varchar](100) NOT NULL,
	[ServiceDescription] [varchar](40) NULL,
	[VisitTypeCode] [varchar](20) NULL,
	[VisitCareLevelCode] [varchar](20) NULL,
	[AdmitDtm] [smalldatetime] NULL,
	[DischargeDtm] [smalldatetime] NULL,
	FOREIGN KEY(PatientID) references Patient(PatientID),
 CONSTRAINT [PK_Visit] PRIMARY KEY CLUSTERED 
(
	[VisitID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
