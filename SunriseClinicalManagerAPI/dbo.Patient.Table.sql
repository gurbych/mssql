SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF OBJECT_ID ('Patient') IS NULL
CREATE TABLE [dbo].[Patient](
	[PatientID] [int] IDENTITY(1, 1) NOT NULL,
	[LastName] [varchar](20) NOT NULL,
	[FirstName] [varchar](20) NOT NULL,
	[middlename] [varchar](20) NULL,
	[gender] [varchar](6) NOT NULL,
	[age] [tinyint] NOT NULL,
	[patientprix] [varchar](50) NULL,
	[Ethnicity] [varchar](30) NULL,
	[Language] [varchar](20) NULL,
	[Race] [varchar](20) NULL,
	[MartialStatus] [varchar](11) NULL,
	[mrn] [int] NOT NULL,
	[ssn] [int] NOT NULL,
	[Occupation] [varchar](20) NULL,
	[Employer] [varchar](20) NULL,
 CONSTRAINT [PK_Patient] PRIMARY KEY CLUSTERED 
(
	[PatientID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
