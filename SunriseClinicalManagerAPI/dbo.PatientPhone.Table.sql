SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID ('PatientPhone') IS NULL
CREATE TABLE [dbo].[PatientPhone](
	[PatientID] [int] IDENTITY(1, 1) NOT NULL,
	[PhoneID] [int] NOT NULL
	FOREIGN KEY (PatientID) references Patient(PatientID),
	FOREIGN KEY (PhoneID) references Phone(PhoneID),
) ON [PRIMARY]

GO
