SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF OBJECT_ID ('Medication') IS NULL
CREATE TABLE [dbo].[Medication](
	[MedicationID] [int] IDENTITY(1, 1) NOT NULL,
	[Medname] [varchar](30) NOT NULL,
	[drugnamefull] [varchar](50) NOT NULL,
	[GPI] [varchar](10) NULL,
	[Drugname] [varchar](30) NOT NULL,
	[GenericName] [varchar](80) NULL,
	[DrugComments] [text] NULL,
 CONSTRAINT [PK_Medication] PRIMARY KEY CLUSTERED 
(
	[MedicationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
