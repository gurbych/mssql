INSERT INTO dbo.Physician (PhysLastName, PhysFirstName) 
VALUES
	('Aaron', 'Gary'),
	('Abbondanza', 'Marco'),
	('?', 'Avicenna'),
	('?', 'Averroes'),
	('Abbot', 'William'),
	('Andrews', 'Matthew'),
	('Apgar', 'Virginia'),
	('Agras', 'William Stewart')