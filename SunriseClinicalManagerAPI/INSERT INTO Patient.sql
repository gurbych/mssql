INSERT INTO dbo.Patient (LastName, FirstName, middlename, gender, age, Ethnicity, Language, Race, MartialStatus, mrn, ssn, Occupation, Employer)
VALUES
	('Kingsleen', 'Mary', NULL, 'female', 34, 'American', 'English', NULL, 'married', 100005, 555555555, 'Genetic Engineer', 'Monsanto'),
	('Farell', 'Tom', NULL, 'male', 22, 'American', 'English', NULL, 'not married', 220403, 666666666, 'Consultant', 'Genetec')