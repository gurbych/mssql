SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF OBJECT_ID ('Location') IS NULL
CREATE TABLE [dbo].[Location](
	[LocationID] [smallint] IDENTITY(1, 1) NOT NULL,
	[CurrentLocationGUID] [varchar](50) NULL,
	[LocationCode] [varchar](20) NULL,
	[ProviderDisplayName] [varchar](40) NULL,
	[LocnGroupName] [varchar](40) NULL,
	[entryname] [varchar](30) NOT NULL,
	[mnemonic] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[LocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
