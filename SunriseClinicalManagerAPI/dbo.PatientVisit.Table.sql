SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID ('PatientVisit') IS NULL
CREATE TABLE [dbo].[PatientVisit](
	[PatientID] [int] IDENTITY(1, 1) NOT NULL,
	[VisitID] [int] NOT NULL
	FOREIGN KEY (PatientID) references Patient(PatientID),
	FOREIGN KEY (VisitID) references Visit(VisitID),
) ON [PRIMARY]

GO
