USE [PopularDictionary]
GO

/****** Object:  Table [dbo].[FirstName]    Script Date: 05.03.2015 14:51:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LastName](
	[LastNameID] [tinyint] IDENTITY(1,1) NOT NULL,
	[LastName] [nvarchar](15) NOT NULL,
	[PDRank] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[LastNameID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


