CREATE PROCEDURE pdCreateTopRankTable
AS
BEGIN
	SET NOCOUNT on;
	IF OBJECT_ID('tempdb..##tempTopRank') IS NOT NULL
		DROP TABLE ##tempTopRank
	CREATE TABLE ##tempTopRank (ColumnName nvarchar(30), RepNumber int)
	INSERT INTO ##tempTopRank (ColumnName, RepNumber)
	SELECT TOP 10 pdCOLUMN, COUNT(*) AS DUPLICATES
	FROM ##tempPersonalDataTable
	GROUP BY pdCOLUMN
	HAVING COUNT(*) > 1
	ORDER BY COUNT(*) DESC
	SELECT * FROM ##tempTopRank
END
GO

EXECUTE pdCreateTopRankTable
GO