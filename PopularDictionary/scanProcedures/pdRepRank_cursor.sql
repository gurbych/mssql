ALTER PROCEDURE topRank
 @db_name nvarchar(128)
AS
BEGIN
DECLARE @s_name nvarchar(128)
DECLARE @t_name nvarchar(128)
DECLARE @c_name nvarchar(128)
DECLARE @count int
DECLARE @countOUT int
DECLARE @str nvarchar(MAX)
DECLARE @ParmDefinition nvarchar(128)

SET @count = 0
SET @countOUT = 0
SET NOCOUNT on;

IF OBJECT_ID('tempdb..##tempTopRank') IS NOT NULL
 DROP TABLE ##tempTopRank
CREATE TABLE ##tempTopRank (pdCOLUMN_NAME nvarchar(30), pdROWS int)

DECLARE pd_cursor CURSOR LOCAL FOR
 SELECT pdSCHEMA, pdTABLE, pdCOLUMN
 FROM ##tempPersonalDataTable
 WHERE pdLevel = 'Critical'

 OPEN pd_cursor
 FETCH NEXT FROM pd_cursor INTO @s_name, @t_name, @c_name
 WHILE @@FETCH_STATUS=0
  BEGIN
   SET @ParmDefinition = N'@countOUT int OUTPUT';

   SET @str = N'SELECT @countOUT = COUNT(*) FROM [' + @db_name + '].[' + @s_name + '].[' + @t_name + ']'
   
   EXECUTE sp_executesql 
    @str
    ,@ParmDefinition
    ,@countOUT = @count OUTPUT

    INSERT INTO ##tempTopRank (pdCOLUMN_NAME, pdROWS) VALUES (@c_name, @count)

   FETCH NEXT FROM pd_cursor INTO @s_name, @t_name, @c_name
  END
 CLOSE pd_cursor
 DEALLOCATE pd_cursor

 SELECT TOP 10 pdCOLUMN_NAME, pdROWS
 FROM ##tempTopRank
 ORDER BY pdROWS DESC
END
GO