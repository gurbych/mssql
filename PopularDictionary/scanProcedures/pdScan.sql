ALTER PROCEDURE searchNames
	@db_name nvarchar(128)
AS
BEGIN
DECLARE @s_name nvarchar(128),
		@t_name nvarchar(128),
		@c_name nvarchar(128),
		@count int,
		@countOUT int,
		@str nvarchar(MAX),
		@ParmDefinition nvarchar(128),
		@totalRows int,
		@currentRow int
DECLARE @tableRows1 TABLE (RowID int IDENTITY(1,1), s_name nvarchar(128), t_name nvarchar(128), c_name nvarchar(128))
DECLARE @tableRows2 TABLE (RowID int IDENTITY(1,1), s_name nvarchar(128), t_name nvarchar(128), c_name nvarchar(128))

SET @count = 0
SET @countOUT = 0

INSERT INTO @tableRows1(s_name, t_name, c_name)
	SELECT s.SCHEMA_NAME, t.TABLE_NAME, c.COLUMN_NAME
	FROM INFORMATION_SCHEMA.TABLES t
	INNER JOIN INFORMATION_SCHEMA.COLUMNS c
	ON c.TABLE_NAME = t.TABLE_NAME
	INNER JOIN INFORMATION_SCHEMA.SCHEMATA s
	ON s.SCHEMA_NAME = t.TABLE_SCHEMA
	WHERE t.TABLE_TYPE = 'BASE TABLE'
	AND c.DATA_TYPE IN ('char', 'varchar', 'text', 'nchar', 'nvarchar', 'ntext')

SELECT @totalRows = @@ROWCOUNT, @currentRow = 1
WHILE @currentRow <= @totalRows
		BEGIN
			SELECT @s_name = a.s_name, @t_name = a.t_name, @c_name  = a.c_name
			FROM @tableRows1 a
			WHERE RowID = @currentRow

			SET @ParmDefinition = N'@countOUT int OUTPUT';

	--FirstName from PopularDictionary.dbo.FirstName

			SET @str = N'SELECT @countOUT = COUNT(*) FROM [' + @db_name + '].[' + @s_name + '].[' + @t_name + '] a
			INNER JOIN PopularDictionary.dbo.FirstName b
			ON b.FirstName = a.[' + @c_name + ']
			COLLATE SQL_Latin1_General_CP1_CI_AS'
   
			EXECUTE sp_executesql 
				@str
				,@ParmDefinition
				,@countOUT = @count OUTPUT
   
			IF (@count > 0)
				BEGIN
					PRINT 'EXISTS'
					UPDATE ##tempPersonalDataTable SET pdLevel = 'Critical' where pdSCHEMA = @s_name AND pdTABLE = @t_name AND pdCOLUMN = @c_name
				END
			ELSE 
				BEGIN 
				PRINT 'DOES NOT EXIST'
					INSERT INTO ##tempPersonalDataTable(pdSCHEMA, pdTABLE, pdCOLUMN, pdLevel) VALUES (@s_name, @t_name, @c_name, 'Critical')
				END

	--LastName from PopularDictionary.dbo.LastName

			SET @str = N'SELECT @countOUT = COUNT(*) FROM [' + @db_name + '].[' + @s_name + '].[' + @t_name + '] a
			INNER JOIN PopularDictionary.dbo.LastName b
			ON b.LastName = a.[' + @c_name + ']
			COLLATE SQL_Latin1_General_CP1_CI_AS'
   
			EXECUTE sp_executesql 
				@str
				,@ParmDefinition
				,@countOUT = @count OUTPUT
   
			IF (@count > 0)
				BEGIN
					PRINT 'EXISTS'
					UPDATE ##tempPersonalDataTable SET pdLevel = 'Critical' where pdSCHEMA = @s_name AND pdTABLE = @t_name AND pdCOLUMN = @c_name
				END
			ELSE 
				BEGIN 
				PRINT 'DOES NOT EXIST'
					INSERT INTO ##tempPersonalDataTable(pdSCHEMA, pdTABLE, pdCOLUMN, pdLevel) VALUES (@s_name, @t_name, @c_name, 'Critical')
				END

	--CityName from PopularDictionary.dbo.CityName

			SET @str = N'SELECT @countOUT = COUNT(*) FROM [' + @db_name + '].[' + @s_name + '].[' + @t_name + '] a
			INNER JOIN PopularDictionary.dbo.CityName b
			ON b.CityName = a.[' + @c_name + ']
			COLLATE SQL_Latin1_General_CP1_CI_AS'
   
			EXECUTE sp_executesql 
				@str
				,@ParmDefinition
				,@countOUT = @count OUTPUT
   
			IF (@count > 0)
				BEGIN
					PRINT 'EXISTS'
					UPDATE ##tempPersonalDataTable SET pdLevel = 'Critical' where pdSCHEMA = @s_name AND pdTABLE = @t_name AND pdCOLUMN = @c_name
				END
			ELSE 
				BEGIN 
				PRINT 'DOES NOT EXIST'
					INSERT INTO ##tempPersonalDataTable(pdSCHEMA, pdTABLE, pdCOLUMN, pdLevel) VALUES (@s_name, @t_name, @c_name, 'Critical')
				END

	--CountryName from PopularDictionary.dbo.CountryName

			SET @str = N'SELECT @countOUT = COUNT(*) FROM [' + @db_name + '].[' + @s_name + '].[' + @t_name + '] a
			INNER JOIN PopularDictionary.dbo.CountryName b
			ON b.CountryName = a.[' + @c_name + ']
			COLLATE SQL_Latin1_General_CP1_CI_AS'
   
			EXECUTE sp_executesql 
				@str
				,@ParmDefinition
				,@countOUT = @count OUTPUT
   
			IF (@count > 0)
				BEGIN
					PRINT 'EXISTS'
					UPDATE ##tempPersonalDataTable SET pdLevel = 'Critical' where pdSCHEMA = @s_name AND pdTABLE = @t_name AND pdCOLUMN = @c_name
				END
			ELSE 
				BEGIN 
				PRINT 'DOES NOT EXIST'
					INSERT INTO ##tempPersonalDataTable(pdSCHEMA, pdTABLE, pdCOLUMN, pdLevel) VALUES (@s_name, @t_name, @c_name, 'Critical')
				END

	--StateName from PopularDictionary.dbo.StateName

			SET @str = N'SELECT @countOUT = COUNT(*) FROM [' + @db_name + '].[' + @s_name + '].[' + @t_name + '] a
			INNER JOIN PopularDictionary.dbo.StateName b
			ON b.StateName = a.[' + @c_name + ']
			COLLATE SQL_Latin1_General_CP1_CI_AS'
   
			EXECUTE sp_executesql 
				@str
				,@ParmDefinition
				,@countOUT = @count OUTPUT
   
			IF (@count > 0)
				BEGIN
					PRINT 'EXISTS'
					UPDATE ##tempPersonalDataTable SET pdLevel = 'Critical' where pdSCHEMA = @s_name AND pdTABLE = @t_name AND pdCOLUMN = @c_name
				END
			ELSE 
				BEGIN 
				PRINT 'DOES NOT EXIST'
					INSERT INTO ##tempPersonalDataTable(pdSCHEMA, pdTABLE, pdCOLUMN, pdLevel) VALUES (@s_name, @t_name, @c_name, 'Critical')
				END

	--StreetName from PopularDictionary.dbo.StreetName

			SET @str = N'SELECT @countOUT = COUNT(*) FROM [' + @db_name + '].[' + @s_name + '].[' + @t_name + '] a
			INNER JOIN PopularDictionary.dbo.StreetName b
			ON b.StreetName = a.[' + @c_name + ']
			COLLATE SQL_Latin1_General_CP1_CI_AS'
   
			EXECUTE sp_executesql 
				@str
				,@ParmDefinition
				,@countOUT = @count OUTPUT
   
			IF (@count > 0)
				BEGIN
					PRINT 'EXISTS'
					UPDATE ##tempPersonalDataTable SET pdLevel = 'Critical' where pdSCHEMA = @s_name AND pdTABLE = @t_name AND pdCOLUMN = @c_name
				END
			ELSE 
				BEGIN 
				PRINT 'DOES NOT EXIST'
					INSERT INTO ##tempPersonalDataTable(pdSCHEMA, pdTABLE, pdCOLUMN, pdLevel) VALUES (@s_name, @t_name, @c_name, 'Critical')
				END

		--RegEx matcing patterns: 1)Phone Number 2)Credit Card Number 3)Zip Code 4)SSN  

			SET @str = N'SELECT @countOUT = COUNT(*) FROM [' + @db_name + '].[' + @s_name + '].[' + @t_name + ']
			WHERE [' + @c_name + '] LIKE ''[0-9][0-9][0-9]_[0-9][0-9][0-9]_[0-9][0-9][0-9][0-9]'' OR 
				[' + @c_name + '] LIKE ''[0-9]_([0-9][0-9])_[0-9][0-9][0-9]_[0-9][0-9][0-9]_[0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''_[0-9][0-9][0-9]_[0-9][0-9][0-9]_[0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''[0-9]_[0-9][0-9][0-9]_[0-9][0-9][0-9]_[0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''_[0-9][0-9]_[0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''[0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''[ABCEGJHKLMNPRSTVXY][0-9][ABCEGJHKLMNPRSTVXYWZ] [0-9][ABCEGJHKLMNPRSTVXYWZ][0-9]'' OR
				[' + @c_name + '] LIKE ''[0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''[0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'''
   
			EXECUTE sp_executesql 
				@str
				,@ParmDefinition
				,@countOUT = @count OUTPUT
   
			IF (@count > 0)
				BEGIN
					PRINT 'EXISTS'
					UPDATE ##tempPersonalDataTable SET pdLevel = 'Critical' where pdSCHEMA = @s_name AND pdTABLE = @t_name AND pdCOLUMN = @c_name
				END
			ELSE 
				BEGIN 
				PRINT 'DOES NOT EXIST'
					INSERT INTO ##tempPersonalDataTable(pdSCHEMA, pdTABLE, pdCOLUMN, pdLevel) VALUES (@s_name, @t_name, @c_name, 'Critical')
				END

			SET @currentRow += 1
		END

INSERT INTO @tableRows2(s_name, t_name, c_name)
	SELECT s.SCHEMA_NAME, t.TABLE_NAME, c.COLUMN_NAME
	FROM INFORMATION_SCHEMA.TABLES t
	INNER JOIN INFORMATION_SCHEMA.COLUMNS c
	ON c.TABLE_NAME = t.TABLE_NAME
	INNER JOIN INFORMATION_SCHEMA.SCHEMATA s
	ON s.SCHEMA_NAME = t.TABLE_SCHEMA
	WHERE t.TABLE_TYPE = 'BASE TABLE'
	AND c.DATA_TYPE IN ('tinyint', 'smallint', 'int', 'bigint')

	SELECT @totalRows = @@ROWCOUNT, @currentRow = 1
	WHILE @currentRow <= @totalRows
		BEGIN
			SELECT @s_name = b.s_name, @t_name = b.t_name, @c_name  = b.c_name
			FROM @tableRows2 b
			WHERE RowID = @currentRow

			SET @ParmDefinition = N'@countOUT int OUTPUT';
		--credit card RegEx - 14,15,16,17,19 numbers + 5 and 9 numbers zip code + phone number (10 digits) + 9 numbers SSN
			SET @str = N'SELECT @countOUT = COUNT(*) FROM [' + @db_name + '].[' + @s_name + '].[' + @t_name + ']
			WHERE [' + @c_name + '] LIKE ''[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'' OR
				[' + @c_name + '] LIKE ''[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'''
   
			EXECUTE sp_executesql 
				@str
				,@ParmDefinition
				,@countOUT = @count OUTPUT
   
			IF (@count > 0)
				BEGIN
					PRINT 'EXISTS'
					UPDATE ##tempPersonalDataTable SET pdLevel = 'Critical' where pdSCHEMA = @s_name AND pdTABLE = @t_name AND pdCOLUMN = @c_name
				END
			ELSE 
				BEGIN 
				PRINT 'DOES NOT EXIST'
					INSERT INTO ##tempPersonalDataTable(pdSCHEMA, pdTABLE, pdCOLUMN, pdLevel) VALUES (@s_name, @t_name, @c_name, 'Critical')
				END

			SET @currentRow += 1
		END
END
GO

EXECUTE searchNames @db_name = N'AdventureWorks2012'
GO