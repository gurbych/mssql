ALTER PROCEDURE spSearchPersonalData
AS
BEGIN
	SET NOCOUNT on;
	IF OBJECT_ID('tempdb..##tempPersonalDataTable') IS NOT NULL
		DROP TABLE ##tempPersonalDataTable
	CREATE TABLE ##tempPersonalDataTable(pdSCHEMA nvarchar(40), pdTABLE nvarchar(40), pdCOLUMN nvarchar(40), pdLevel varchar(12))

	INSERT INTO ##tempPersonalDataTable(pdSCHEMA, pdTABLE, pdCOLUMN, pdLevel)
	SELECT s.TABLE_SCHEMA, t.name, c.name, pdLevel = 'Critical'
	FROM INFORMATION_SCHEMA.TABLES s
	INNER JOIN sys.tables t
	ON s.TABLE_NAME = t.name
	INNER JOIN sys.columns c 
	ON t.object_id = c.object_id
	WHERE c.name LIKE '%[Pp]erson%' OR
		c.name LIKE '%[Aa]ddress%' OR
		c.name LIKE '%[Cc]ity%' OR
		c.name LIKE '%[Ss]tate%' OR
		c.name LIKE '%[Dd]ate%' OR
		c.name LIKE '%[Cc]redit%[Cc]ard%' OR
		c.name LIKE '%[Cc]redit%' OR
		c.name LIKE '%[Cc]ard%' OR
		--c.name LIKE '%[C,c][C,c]%' OR
		c.name LIKE '%[Ss]ocial%[Ss]ecurity%[Nn]umber%' OR
		c.name LIKE '%[Ss][Ss][Nn]%' OR
		c.name LIKE '%[Ss]%[Ss]%[Nn]um%' OR
		c.name LIKE '%[Ss]oc[Ss]ec%' OR
		c.name LIKE '%[Aa]ccount%' OR
		c.name LIKE '%[Ff]ax%' OR
		c.name LIKE '%[Pp]hone%' OR
		c.name LIKE '%[Ll]icense%' OR
		c.name LIKE '%[Vv]ehicle%' OR
		c.name LIKE '%[Vv][Ii][Nn]%' OR
		c.name LIKE '%[Rr]ecord%' OR
		c.name LIKE '%[Pp]atient%' OR
		c.name LIKE '%[Bb]ank%' OR
		c.name LIKE '%[Rr]oute%' OR
		c.name LIKE '%[Pp][Ii][Nn]%' OR
		c.name LIKE '%[Nn][Aa][Mm][Ee]%' OR

		--Medication
		--c.name LIKE '%[M,m]ed%[N,n]ame%' OR
		--c.name LIKE '%[D,d]rug%[N,n]ame%[F,f]ull%' OR
		c.name LIKE '%[Gg][Pp][Ii]%' OR
		--c.name LIKE '%[D,d]rug%[N,n]ame%' OR
		--c.name LIKE '%[G,g]eneric%[N,n]ame%' OR
		c.name LIKE '%[Dd]rug%[Cc]omment%' OR

		--Vitals
		c.name LIKE '%[Rr]ecorded%[Dd][Tt][Mm]%' OR
		c.name LIKE '%[Vv]alue%' OR
		c.name LIKE '%[Dd]escription%' OR
		c.name LIKE '%[Uu]nit%[Oo]f%[Mm]easure%' OR
		c.name LIKE '%[Rr]ight%[Jj]ustified%[Ll]abel%' OR
		c.name LIKE '%[Ll]eft%[Jj]ustified%[Ll]abel%' OR

		--Task
		--c.name LIKE '%[P,p]at%[N,n]ame%' OR
		c.name LIKE '%[Tt]ask%[Oo]wner%' OR

		--Patient
		--c.name LIKE '%[L,l]ast%[N,n]ame%' OR
		--c.name LIKE '%[F,f]irst%[N,n]ame%' OR
		--c.name LIKE '%[M,m]iddle%[N,n]ame%' OR
		c.name LIKE '%[Gg]ender%' OR
		c.name LIKE '%[Aa]ge%' OR
		c.name LIKE '%[Pp]atient%[P,p]ix%' OR
		c.name LIKE '%[Ee]thnicity%' OR
		c.name LIKE '%[Ll]anguage%' OR
		c.name LIKE '%[Rr]ace%' OR
		c.name LIKE '%[Mm]artial%[Ss]tatus%' OR
		c.name LIKE '%[Mm][Rr][Nn]%' OR
		c.name LIKE '%[Ee]mployer%' OR
		c.name LIKE '%[Oo]ccupation%' OR
		c.name LIKE '%[Gg]ender%' OR

		--PhoneType
		--c.name LIKE '%[Cc]ell%[Pp]hone%' OR
		--c.name LIKE '%[Pp]hone%[Nn]umber%' OR
		--c.name LIKE '%[Ww]ork%[Pp]hone%' OR
		--c.name LIKE '%[Hh]ome%[Pp]hone%' OR

		--Address
		c.name LIKE '%[Zz]ip%[Cc]ode%' OR
		--c.name LIKE '%[Aa]ddress%[Ll]ine%' OR

		--Visit
		--c.name LIKE '%[C,c]lient%[D,d]isplay%[N,n]ame%' OR
		c.name LIKE '%[Vv]isit%[Ss]tatus%' OR
		c.name LIKE '%[Aa]dmit%[Dd][Tt][Mm]%' OR
		c.name LIKE '%[Dd]ischarge%[Dd][Tt][Mm]%' OR
		--c.name LIKE '%[P,p]rovider%[D,d]isplay[N,n]ame%' OR
		c.name LIKE '%[Vv]isit%[Rr]eason%' OR
		c.name LIKE '%[Ss]ervice%[Dd]escription%' OR
		c.name LIKE '%[Vv]isit%[Tt]ype[Cc]ode%' OR
		c.name LIKE '%[Vv]isit%[Cc]are%[Ll]evel%[Cc]ode%' OR

		--Location
		c.name LIKE '%[Ll]ocation%' OR
		--c.name LIKE '%[Cc]urrent%[Ll]ocation%' OR
		--c.name LIKE '%[L,l]ocn%[G,g]roup[N,n]ame%' OR
		--c.name LIKE '%[E,e]ntry%[N,n]ame%' OR
		c.name LIKE '%[Mm]nemonic%'

		SELECT @@ROWCOUNT AS FOUND
	UPDATE ##tempPersonalDataTable
	SET pdLevel = 'non-critical'
	WHERE pdCOLUMN LIKE '%[Mm]odif%[Dd]ate%' OR
		pdCOLUMN LIKE '%[Dd]ue%[Dd]ate%' OR
		pdCOLUMN LIKE '%[Vv]er%[Dd]ate%' OR
		pdCOLUMN LIKE '%[Oo]rder%[Dd]ate%' OR
		pdCOLUMN LIKE '%[Ee]nd%[Dd]ate%' OR
		pdCOLUMN LIKE '%[Ss]tart%[Dd]ate%' OR
		pdCOLUMN LIKE '%[Uu]pdat%[Dd]ate%' OR
		pdCOLUMN LIKE '%[Ii]nsert%[D,d]ate%' OR
		pdCOLUMN LIKE '%[Aa]rchi%[Dd]ate%' OR
		pdCOLUMN LIKE '%[Cc]urren%[Dd]ate%' OR
		pdCOLUMN LIKE '%[Dd]eliver%[Dd]ate%' OR
		pdCOLUMN LIKE '%[Rr]eview%[Dd]ate%' OR
		pdCOLUMN LIKE '%[Rr]eceipt%[Dd]ate%' OR
		pdCOLUMN LIKE '%[Dd]ate%[Cc]reat%' OR
		pdCOLUMN LIKE '%[Ss]hip%[Dd]ate%' OR
		pdCOLUMN LIKE '%ID' OR
		pdCOLUMN LIKE '%[Ee]rror%'

		SELECT @@ROWCOUNT AS MODIFIED
	SELECT * FROM ##tempPersonalDataTable
	ORDER BY pdSCHEMA, pdTABLE, pdCOLUMN
END
GO

EXECUTE spSearchPersonalData
GO