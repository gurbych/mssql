ALTER PROCEDURE topRank
	@db_name nvarchar(128)
AS
BEGIN
DECLARE @s_name nvarchar(128),
	    @t_name nvarchar(128),
        @c_name nvarchar(128),
		@count int,
        @countOUT int,
        @str nvarchar(MAX),
        @ParmDefinition nvarchar(128),
		@totalRows int,
		@currentRow int
DECLARE @tableRows TABLE (RowID int IDENTITY(1,1), s_name nvarchar(128), t_name nvarchar(128), c_name nvarchar(128))

SET @count = 0
SET @countOUT = 0
SET NOCOUNT on;

IF OBJECT_ID('tempdb..##tempTopRank') IS NOT NULL
	DROP TABLE #tempTopRank
CREATE TABLE #tempTopRank (pdCOLUMN_NAME nvarchar(30), pdROWS int)

INSERT INTO @tableRows(s_name, t_name, c_name)
	SELECT pdSCHEMA, pdTABLE, pdCOLUMN
	FROM #tempPersonalDataTable
	WHERE pdLevel = 'Critical'

SELECT @totalRows = @@ROWCOUNT, @currentRow = 1
WHILE @currentRow <= @totalRows
		BEGIN
			SELECT @s_name = a.s_name, @t_name = a.t_name, @c_name  = a.c_name
			FROM @tableRows a
			WHERE RowID = @currentRow

			SET @ParmDefinition = N'@countOUT int OUTPUT';
			SET @str = N'SELECT @countOUT = COUNT(*) FROM [' + @db_name + '].[' + @s_name + '].[' + @t_name + ']'
   
			EXECUTE sp_executesql 
				@str
				,@ParmDefinition
				,@countOUT = @count OUTPUT

			INSERT INTO #tempTopRank (pdCOLUMN_NAME, pdROWS) VALUES (@c_name, @count)
			SET @currentRow += 1
		END

	SELECT TOP 10 pdCOLUMN_NAME, pdROWS
	FROM #tempTopRank
	ORDER BY pdROWS DESC
END
GO

EXECUTE topRank 'AdventureWorks2012'
GO