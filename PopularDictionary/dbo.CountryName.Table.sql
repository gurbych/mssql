USE [PopularDictionary]
GO

/****** Object:  Table [dbo].[FirstName]    Script Date: 05.03.2015 14:51:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CountryName](
	[CountryNameID] [smallint] IDENTITY(1,1) NOT NULL,
	[CountryName] [nvarchar](40) NOT NULL,
	[CountryCode] [smallint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CountryNameID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


